import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    admin: null,
    isError: false,
    IsSuccess: false,
    isLoading: false,
    message:""
}

export const LoginAdmin = createAsyncThunk("admin/LoginAdmin", async(admin, thunkAPI) => {
    try {
        const response = await axios.post('https://ryanariana6.my.id/login', {
            email: admin.email,
            password: admin.password
        });
        console.log(response.data);
        return response.data;
    } catch (error) {
        if(error.response){
            const message = error.response.data.msg;
            return thunkAPI.rejectWithValue(message);
        }
    }
});

export const getMe = createAsyncThunk("admin/getMe", async(_ , thunkAPI) => {
    try {
        const response = await axios.get('https://ryanariana6.my.id/me', {
            headers: {
                'Cookie': 'connect.sid=s%3AlziWvUU-3Tdd9Ml3mnJ0v54lgl_gz6Xj.Kjydm5OEgn7ZakA9SYqGKb9w%2B%2BGG4QduZZqHmm6QrAk; Path=/; HttpOnly; SameSite=None; connect.sid=s%3AGb1KLxJiyu4DMlSuz9Ftd4WUgUtRbTaR.Ht508iFD2FL3qZtUsDcgJIGQVQ3FMTB36wqnE37iYA4'
            },
            withCredentials: true // Mengirim kredensial (termasuk cookie) dengan permintaan
        });
        return response.data;
    } catch (error) {
        if(error.response){
            const message = error.response.data.msg;
            return thunkAPI.rejectWithValue(message);
        }
    }
});

export const LogOut = createAsyncThunk("admin/LogOut", async() => {
    await axios.delete('https://ryanariana6.my.id/logout');
});

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        reset: (state) => initialState
    },
    extraReducers:(builder) =>{
        builder.addCase(LoginAdmin.pending, (state) => {
            state.isLoading = true;
        });
        builder.addCase(LoginAdmin.fulfilled, (state, action) => {
            state.isLoading = false;
            state.IsSuccess= true;
            state.admin = action.payload;
        });
        builder.addCase(LoginAdmin.rejected, (state, action) => {
            state.isLoading = false;
            state.isError= true;
            state.message = action.payload;
        })

        //get login
        builder.addCase(getMe.pending, (state) => {
            state.isLoading = true;
        });
        builder.addCase(getMe.fulfilled, (state, action) => {
            state.isLoading = false;
            state.IsSuccess= true;
            state.admin = action.payload;
        });
        builder.addCase(getMe.rejected, (state, action) => {
            state.isLoading = false;
            state.isError= true;
            state.message = action.payload;
        })
    }
});

export const {reset} = authSlice.actions;
export default authSlice.reducer;