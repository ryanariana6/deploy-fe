import React, {useState, useEffect} from 'react';
import axios from "axios";
import './App.css';
import NavbarCom from "../../inc/NavbarCom";
import Footer from "../../inc/Footer";


const Alat = () => {

    const [Alat, setAlat] = useState([]);
    
    useEffect(() =>{
        getAlat();
    },[]);

    const getAlat = async () => {
        const response = await axios.get('https://ryanariana6.my.id/tools');
        setAlat(response.data);
    };

    return (
        <div>
            <NavbarCom/>
            <section className='py-3 bg-info'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-4 my-auto'>
                            <h4> Alat </h4>
                        </div>
                        <div className='col-md-8 my-auto'>
                            <h6 className='float-end'> Home /  Alat </h6>
                        </div>
                    </div>
                </div>
            </section> 

            <section className='section'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-12 text-center'>
                            <h3 className='main-heading'> Alat-Alat KAMI </h3>
                            <div className='underline mx-auto'></div>
                            <p className='pTit'>" Pekerja PT. Layar Perkasa Nusantara memiliki sertifikasi dan kualifikasi sesuai bidang Perkapalan "
                            </p>
                            <p className='pTit'>" Kami bekerja untuk KEPUASAN ANDA dan kepuasan Anda Adalah PRIORITAS UTAMA KAMI "
                            </p>
                        </div>
                    </div>
                </div>
            </section>


            <section className='section border-top'>
                <div className='container'>

                    {/* Card 1 */}
                    {Alat.map((alat, index) => (
                    <div className='row mb-6'>
                        <div className='col-md-4'>
                            <div className='card shadow'>
                                <img src={`https://ryanariana6.my.id/images/imgAlat/${alat.imageAlat}`} className='imgser' alt='Services' />
                            </div>
                        </div>
                        <div className='card-body col-md-7 ml-3'>
                                    <h6> {`${alat.nameAlat}`} </h6>
                                    <div className='underline'></div>
                                    <p> {`${alat.ketAlat}`} </p>
                        </div>
                    </div>
                    ))}

                </div>
            </section>

        <Footer/>
        </div>
    )
}

export default Alat