import React, {useState, useEffect} from 'react';
import axios from "axios";
import './App.css';
import NavbarCom from "../../inc/NavbarCom";
import Footer from "../../inc/Footer";
import moment from 'moment';
import 'moment/locale/id';

const MainAlat = () => {

    const [Alat, setAlat] = useState([]);
    
    useEffect(() =>{
        getAlat();
    },[]);

    const getAlat = async () => {
        const response = await axios.get('https://ryanariana6.my.id/tools');
        setAlat(response.data);
    };

    return (
        <div>
            <NavbarCom/>
            <section className='py-3 bg-info'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-4 my-auto'>
                            <h4> Alat </h4>
                        </div>
                        <div className='col-md-8 my-auto'>
                            <h6 className='float-end'> Home /  Alat </h6>
                        </div>
                    </div>
                </div>
            </section> 

            <section className='section'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-12 text-center'>
                            <h3 className='main-heading'> Alat-Alat KAMI </h3>
                            <div className='underline mx-auto'></div>
                            <p className='pTit'>" Pekerja PT. Layar Perkasa Nusantara memiliki sertifikasi dan kualifikasi sesuai bidang Perkapalan "
                            </p>
                            <p className='pTit'>" Kami bekerja untuk KEPUASAN ANDA dan kepuasan Anda Adalah PRIORITAS UTAMA KAMI "
                            </p>
                        </div>
                    </div>
                </div>
            </section>


            <section className='section border-top'>
                <div className='container'>
                <div className='row'>
                    {/* Card 1 */}
                    {Alat.map((alat, index) => (
                        <div key={alat.id} className='col-md-4 '>
                            <div className='card shadow cardImage'>
                                <div className='card-body'>
                                    <img src={`https://ryanariana6.my.id/images/imgAlat/${alat.imageAlat}`} className='imgBer border-bottom' alt='...' />
                                    <h1 className='title text-center mt-2 text-dark'> {`${alat.nameAlat}`} </h1>
                                    <p className='text-dark'>
                                        {alat.ketAlat}
                                    </p>
                                    <p>{"Tanggal Dibuat : " + moment(alat.createdAt).format('dddd, DD MMMM YYYY')}</p>
                                </div>
                            </div>
                        </div>
                        ))}
                        </div>
                </div>
            </section>

        <Footer/>
        </div>
    )
}

export default MainAlat