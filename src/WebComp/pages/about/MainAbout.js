import React, {useState, useEffect} from 'react';
import axios from "axios";
import './App.css';
import NavbarCom from "../../inc/NavbarCom";
import Footer from "../../inc/Footer";

const MainAbout = () => {

    const [About, setAbout] = useState([]);
    // console.log(About)
    
    useEffect(() =>{
        getProfil();
    },[]);

    const getProfil = async () => {
        const response = await axios.get('https://ryanariana6.my.id/abouts');
        setAbout(response.data);
    };

    function RenderList({ list }) {
        return <>
            {list.map((text, index) => <p key={index}>{text}</p>)}
        </>
    }
    
    return (
        <div>
        <NavbarCom/>
            <section className='py-3 bg-info'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-4 my-auto'>
                            <h4> About </h4>
                        </div>
                        <div className='col-md-8 my-auto'>
                            <h6 className='float-end'> Home /  About </h6>
                        </div>
                    </div>
                </div>
            </section> 

            <section className='section'>
                <div className='container'>
                <h3 className='main-heading text-center'> SEJARAH </h3>

                {About.map((about, index) => (
                    <div className='row' key={index}>
                        <div className='col-md-12'>
                            <div className='underline mx-auto'></div>
                            <RenderList list={about.sejarah.split("<p>")}/>
                        </div>
                    </div>
                    ))}

                
                    <div className='row mt-5'>
                        <div className='col-md-12 text-center'>
                            <h3 className='main-heading'> Visi, Misi Dan Nilai-Nilai </h3>
                            <div className='underline mx-auto'></div>
                        </div>

                        {About.map((about, index) => (
                        <div className='col-md-4 'key={index}>
                            <h5 className='vismisval text-center'> Visi </h5>
                            <RenderList list={about.visi.split("<p>")}/>
                        </div>
                        ))}




                        {About.map((about, index) => (
                        <div className='col-md-4 'key={index}>
                            <h5 className='vismisval text-center'> Misi </h5>
                            <RenderList list={about.misi.split("<p>")}/>
                        </div>
                        ))}




                        {About.map((about, index) => (
                        <div className='col-md-4 'key={index}>
                            <h5 className='vismisval text-center'> Nilai-Nilai </h5>
                            <RenderList list={about.nilai.split("<p>")}/>
                        </div>
                        ))}

                    </div>

                </div>
            </section>

        <Footer/>
        </div>
    )
}

export default MainAbout;