import React from 'react';
import './App.css';

import ReactMarkdown from 'react-markdown';

const MainAbout = (props) => {
        return(
            <ReactMarkdown components={{
                p: 'p',
            }}>
                {props.description}
            </ReactMarkdown>
        )
    };

export default MainAbout;