import React, {useState, useEffect} from 'react';
import Carousel from 'react-bootstrap/Carousel';
import axios from "axios";

const Slider = () => {

    const [Heros, setHeros] = useState([]);
    
    useEffect(() =>{
        getHero();
    },[]);

    const getHero = async () => {
        const response = await axios.get('https://ryanariana6.my.id/heros');
        setHeros(response.data);
    };

    return(
        <Carousel data-bs-theme="light">
            {Heros.map((hero, index) => (
            <Carousel.Item key={index}>
                <img
                className="d-block w-100"
                src={`https://ryanariana6.my.id/images/imgHero/${hero.imageHero}`}
                alt="First slide"
                />
                <Carousel.Caption>
                <h5>{`${hero.nameHero}`}</h5>
                <p className='text-light'>{`${hero.ketHero}`}</p>
                </Carousel.Caption>
            </Carousel.Item>
            ))}
            </Carousel>
            
    );
}

export default Slider;