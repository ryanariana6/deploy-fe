import React from 'react';
import './App.css';
import {Link} from 'react-router-dom';
import Logo from '../../logolpn.png'

const Footer = () => {
    return (
        // <div>
            <section className='section footer bg-dark text-white'>
                <div className='containter text-center'>
                    <div className='row'>
                        <div className='col-md-3'>
                        <img src={Logo} className='logoFoo' alt='logo'/>
                            <p className='text-white'>
                            PT. LPN Shipyard memiliki area yang luas dan dilengkapi peralatan terbaik untuk memastikan semua kebutuhan layanan Kapal Anda terpenuhi seluruhnya.
                            </p>
                        </div>
                        <div className='col-md-3'>
                            <h6> Visitor </h6>
                            <hr/>
                        <a href="http://s11.flagcounter.com/more/0kH"><img src="https://s11.flagcounter.com/count2/0kH/bg_FFFFFF/txt_000000/border_CCCCCC/columns_2/maxflags_12/viewers_0/labels_1/pageviews_0/flags_0/percent_0/" alt="Flag Counter" border="0"/></a>
                        </div>
                        <div className='col-md-3'>
                            <h6> Jam Kerja </h6>
                            <hr/>
                            Monday - Friday : <span className="text-right"> 08:00 - 16:00 </span>
                            <br/>
                            Saturday - Sunday : <span className="text-right"> 08:00 - 16:00 </span>
                        </div>
                        <div className='col-md-3'>
                            <h6> Link </h6>
                            <hr/>
                            <div><Link to='/'>Home</Link></div>
                            <div><Link to='/layanan'>Layanan</Link></div>
                            <div><Link to='/tools'>Tools</Link></div>
                            <div><Link to='/about'>About</Link></div>
                            <div><Link to='/contact'>Contact</Link></div>
                        </div>
                    </div>
                </div>
            </section>
        // </div>
    )
}

export default Footer