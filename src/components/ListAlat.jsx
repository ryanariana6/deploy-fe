import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Swal from 'sweetalert2';
import moment from 'moment';
import 'moment/locale/id';

const ListAlat = () => {
    const [Alat, SetAlat] = useState([]);
    
    useEffect(() =>{
        getAlat();
    },[]);

    const getAlat = async () => {
        const response = await axios.get('https://ryanariana6.my.id/tools');
        SetAlat(response.data);
    };

    const deleteAlat = async (alatId) => { 
        await axios.delete(`https://ryanariana6.my.id/tools/${alatId}`);
        getAlat();
    };

    const deleteProses  = async (alatId) => {
        try {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    deleteAlat(alatId);
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                }
            })
        } catch (error) {
            
        }
    };
    return (
    <div className="py-4">
        <h1 className="title text-dark"> Alat </h1>
        <h2 className="subtitle text-dark"> List Alat </h2>
        <Link to="/admin/tools/add" className="button is-primary mb-2 text-dark">Add New </Link>
        <table className="table is-striped is-fullwidth text-dark">
            <thead className="text-dark">
                <tr className="text-dark">
                    <th className="text-dark">No</th>
                    <th className="text-dark">Nama</th>
                    <th className="text-dark">Images</th>
                    <th className="text-dark">keterangan</th>
                    <th className="text-dark">Dibuat Oleh</th>
                    <th className="text-dark">Dibuat Pada</th>
                    <th className="text-dark">Diubah Pada</th> 
                    <th className="text-dark">Actions</th>
                </tr>
            </thead>
            <tbody>
            {Alat.map((alat, index) => (
                <tr key={alat.uuidAlat}>
                    <td>{index + 1}</td>
                    <td>{alat.nameAlat}</td>
                    <td><img src={`https://ryanariana6.my.id/images/imgAlat/${alat.imageAlat}`} alt="No images" width="200" height="300"/></td>
                    <td className="text-break"><details>{alat.ketAlat}</details></td>
                    <td>{alat.user.name}</td>
                    <td>{moment(alat.createdAt).format('dddd, DD MMMM YYYY')}</td>
                    <td>{moment(alat.updatedAt).format('DD MMMM YYYY')}</td>
                    <td>
                        <Link to={`/admin/tools/edit/${alat.id}`} className="button is-small is-info">Edit</Link>
                        <button onClick={()=> deleteProses(alat.id)} className="button is-small is-danger">Hapus</button>
                    </td>
                </tr>
                ))}
            </tbody>
        </table>
    </div>
    );
};

export default ListAlat;