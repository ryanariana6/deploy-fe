import React from "react";
import { NavLink , useNavigate } from "react-router-dom";
import { IoPerson, IoPricetag, IoHome, IoLogOut } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { LogOut, reset } from "../features/authSlice";


const Sidebar = () => {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {admin} = useSelector((state) => state.auth);

    const logout = () =>{
        dispatch(LogOut());
        dispatch(reset());
        navigate("/admin/login");
    };

return (
    <div className="">
    <div className="py-4">
    <aside className="shadow-none p-2 mb-5 rounded ">
    <p className="menu-label pl-1 mb-3 text-light">
        General
    </p>
        <ul className="menu-list pl-3">
            <li >
                <NavLink to={"/admin/dashboard"}className="text-light mb-2"><IoHome/>Dashboard</NavLink>
            </li>
            <li >
                <NavLink to={"/admin/heros"}className="text-light mb-2"><IoHome/>Hero</NavLink>
            </li>
            <li>
                <NavLink to={"/admin/news"}className="text-light mb-2"><IoPricetag/>News</NavLink>
            </li>
            <li>
                <NavLink to={"/admin/abouts"}className="text-light mb-2"><IoPricetag/>About</NavLink>
            </li>
            <li>
                <NavLink to={"/admin/services"}className="text-light mb-2"><IoPricetag/>Service</NavLink>
            </li>
            <li>
                <NavLink to={"/admin/tools"}className="text-light mb-2"><IoPricetag/>Tools</NavLink>
            </li>
        </ul>
        {admin && admin.role === "admin" && (
            <div>
                <p className="menu-label pl-1 text-light mb-3">Admin</p>
                <ul className="menu-list pl-3">
                <li>
                <NavLink to={"/admin/admins"}className="text-light mb-2"><IoPerson/>Admin</NavLink>
                </li>
                </ul>
            </div>
        )}
    
    <p className="menu-label pl-1 text-light mb-3">
    Settings
    </p>
        <ul className="menu-list pl-3">
            <li>
                <button onClick={logout} className="button is-white mb-2 text-light "><IoLogOut/>Logout</button>
            </li>
        </ul>
    </aside>
    </div>
    </div>
);
};

export default Sidebar;