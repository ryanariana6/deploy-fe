import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddAdmin = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confPassword, setConfPassword] = useState("");
    const [role, setRole] = useState("");
    const [msg, setMsg] = useState("");
    const navigate = useNavigate();

    const saveAdmin = async (e) => {
        e.preventDefault();
        try {
            await axios.get(`https://ryanariana6.my.id/admins/email/${email}`)
                .then(function (response) {
                    if (response.data.msg !== true) {
                        console.log('belum ada')
                        axios.post('https://ryanariana6.my.id/admins', {
                            name: name,
                            email: email,
                            password: password,
                            confPassword: confPassword,
                            role: role
                        }).then(function (response) {
                            // handle success
                            navigate("/admin/admins");
                        })
                    } else {
                        setMsg("Email Sudah Ada!");
                    }
                })

        } catch (error) {
            if (error.response) {
                setMsg(error.response.data.msg);
            }
        }
    };

    return (
        <div className="py-4">
            <h1 className="title text-dark"> Admin </h1>
            <h2 className="subtitle text-dark"> Add New Id</h2>
            <div className="card is-shadowless">
                <div className="card-content">
                    <div className="content">
                        <form onSubmit={saveAdmin}>
                            <p className="has-text-centered">{msg}</p>
                            <div className="field">
                                <label className="label text-dark"> Nama </label>
                                <div className="control">
                                    <input
                                        type="text"
                                        className="input bg-light text-dark"
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                        placeholder="Name"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label text-dark"> Email </label>
                                <div className="control">
                                    <input
                                        type="text"
                                        className="input bg-light text-dark"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        placeholder="Email"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label text-dark"> Password </label>
                                <div className="control">
                                    <input
                                        type="password"
                                        className="input bg-light text-dark"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        placeholder="******"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label text-dark"> Confirm Password </label>
                                <div className="control">
                                    <input
                                        type="password"
                                        className="input bg-light text-dark"
                                        value={confPassword}
                                        onChange={(e) => setConfPassword(e.target.value)}
                                        placeholder="******"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label text-dark"> Role </label>
                                <div className="control">
                                    <div className="select is-fullwidth ">
                                        <select className="bg-light text-dark" value={role} onChange={(e) => setRole(e.target.value)}>
                                            <option value="admin">Admin</option>
                                            <option value="Adminlpn">Adminlpn</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="field">
                                <div className="control">
                                    <button type="submit" className="button is-success ">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FormAddAdmin