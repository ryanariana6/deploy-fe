import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Swal from 'sweetalert2';
import moment from 'moment';
import 'moment/locale/id';

const ListAbout = () => {
    const [About, setAbout] = useState([]);
    
    useEffect(() =>{
        getProfil();
    },[]);

    const getProfil = async () => {
        const response = await axios.get('https://ryanariana6.my.id/abouts');
        setAbout(response.data);
    };

    const deleteProfil = async (AboutId) => { 
        await axios.delete(`https://ryanariana6.my.id/abouts/${AboutId}`);
        getProfil();
    };

    const deleteProses  = async (AboutId) => {
        try {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    deleteProfil(AboutId);
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                }
            })
        } catch (error) {
            
        }
    };

    return (
        <div className="py-4">
        <h1 className="title text-dark"> Profil </h1>
        <h2 className="subtitle text-dark"> List Profil </h2>
        <Link to="/admin/abouts/add" className="button is-primary mb-2">Add New</Link>
        <table className="table is-striped is-fullwidth">
            <thead className="text-dark">
                <tr className="text-dark">
                    <th className="text-dark">No</th>
                    <th className="text-dark">sejarah</th>
                    <th className="text-dark">visi</th>
                    <th className="text-dark">misi</th>
                    <th className="text-dark">nilai</th>
                    <th className="text-dark">Dibuat Oleh</th>
                    <th className="text-dark">Dibuat Pada</th>
                    <th className="text-dark">Diubah Pada</th>
                    <th className="text-dark">Actions</th>
                </tr>
            </thead>
            <tbody>
            {About.map((about, index) => (
                <tr key={about.uuid}>
                    <td>{index + 1}</td>
                    <td><details>{about.sejarah}</details></td>
                    <td><details>{about.visi}</details></td>
                    <td><details>{about.misi}</details></td>
                    <td><details>{about.nilai}</details></td>
                    <td>{about.user.name}</td>
                    <td>{moment(about.createdAt).format('dddd, DD MMMM YYYY')}</td>
                    <td>{moment(about.updatedAt).format('DD MMMM YYYY')}</td>
                    <td>
                        <Link to={`/admin/abouts/edit/${about.uuid}`} className="button is-small is-info">Edit</Link>
                        <button onClick={()=> deleteProses(about.uuid)} className="button is-small is-danger">Hapus</button>
                    </td>
                </tr>
                ))}
            </tbody>
        </table>
    </div>
    );
};

export default ListAbout;