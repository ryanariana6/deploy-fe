import React, {useState} from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux/es/hooks/useSelector";

const FormAddBerita = () => {
    const [title, setName] = useState("");
    const [file, setFile] = useState("");
    const [keterangan, setKet] = useState("");
    const [msg, setMsg] = useState("");
    const [preview, setPreview] = useState("");
    const navigate = useNavigate();
    const {admin} = useSelector((state) => state.auth);
    console.log(admin, 'login sebagai')
    const loadImage = (e) => {
        const image = e.target.files[0];
        setFile(image);
        setPreview(URL.createObjectURL(image));
    };

    const saveBerita = async(e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("title", title);
        formData.append("file", file);
        formData.append("ketBerita", keterangan);
        formData.append("userId", admin.id);
        try {
            await axios.post("https://ryanariana6.my.id/news", formData,{
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            navigate("/admin/news");
        } catch (error) {
            if(error.response) {
                setMsg(error.response.data.msg);
            }
        }
    };

return (
    <div className="py-4">
        <h1 className="title text-dark"> Berita </h1>
        <h2 className="subtitle text-dark"> Add New Berita</h2>
        <div className="card is-shadowless">
            <div className="card-content">
                <div className="content">
                <form onSubmit={saveBerita}>
                <p className="has-text-centered">{msg}</p>
                    <div className="field">
                        <label className="label text-dark"> Judul Berita </label>
                        <div className="control">
                            <input
                            type="text"
                            className="input bg-light text-dark"
                            value={title} 
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Judul Berita"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label text-dark"> Images </label>
                        <div className="control">
                            <div className="file">
                                <label className="file-label">
                                    <input type="file" className="file-input" onChange={loadImage}/>
                                    <span className="file-cta">
                                    <span className="file-label">Choose a File </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    {preview ?(
                    <figure className="image is-128x128">
                        <img src={preview} alt="preview images" />
                    </figure>
                    ):(
                    ""
                    )}
                    <div className="field">
                        <label className="label text-dark"> Keterangan </label>
                        <div className="control">
                            <input
                            type="text"
                            className="input bg-light text-dark"
                            value={keterangan} 
                            onChange={(e) => setKet(e.target.value)}
                            placeholder="Keterangan"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label text-dark"> Role </label>
                            {admin && admin.name}
                    </div>
                    <div className="field">
                        <div className="control">
                            <button type="submit" className="button is-success ">Save</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
);
};

export default FormAddBerita;