import React, {useState , useEffect} from "react";
import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux/es/hooks/useSelector";

const FormEditAlat = () => {
    const [name, setName] = useState("");
    const [file, setFile] = useState("");
    const [keterangan, setKetAlat] = useState("");
    const [msg, setMsg] = useState("");
    const [preview, setPreview] = useState("");
    const {id} = useParams();
    const navigate = useNavigate();
    const {admin} = useSelector((state) => state.auth);


    useEffect(() => {
        const getAlatById = async () => {
            try {
                const response = await axios.get(`https://ryanariana6.my.id/tools/${id}`
                );
                setName(response.data.nameAlat);
                setFile(response.data.image);
                setKetAlat(response.data.ketAlat);
            } catch (error) {
                if (error.response){
                    setMsg(error.response.data.msg);
                }
            }
        };
        getAlatById();
    }, [id]);

    const loadImage = (e) => {
        const image = e.target.files[0];
        setFile(image);
        setPreview(URL.createObjectURL(image));
    };

    const updateAlat = async(e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("title", name);
        formData.append("file", file);
        formData.append("ketAlat", keterangan);
        try {
            await axios.patch(`https://ryanariana6.my.id/tools/${id}`, formData,{
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            navigate("/admin/tools");
        } catch (error) {
            if(error.response) {
                setMsg(error.response.data.msg);
            }
        }
    };

return (
    <div className="py-4">
        <h1 className="title text-dark"> Alat </h1>
        <h2 className="subtitle text-dark"> Add New Alat </h2>
        <div className="card is-shadowless">
            <div className="card-content">
                <div className="content">
                <form onSubmit={updateAlat}>
                <p className="has-text-centered">{msg}</p>
                    <div className="field">
                        <label className="label text-dark"> Alat </label>
                        <div className="control">
                            <input
                            type="text"
                            className="input bg-light text-dark"
                            value={name} 
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Nama Alat"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label text-dark"> Images </label>
                        <div className="control">
                            <div className="file">
                                <label className="file-label">
                                    <input type="file" className="file-input" onChange={loadImage}/>
                                    <span className="file-cta">
                                    <span className="file-label text-light">Choose a File </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    {preview ?(
                    <figure className="image is-128x128">
                        <img src={preview} alt="preview images" />
                    </figure>
                    ):(
                    ""
                    )}
                    <div className="field">
                        <label className="label text-dark"> Keterangan </label>
                        <div className="control">
                            <input
                            type="text"
                            className="input bg-light text-dark"
                            value={keterangan} 
                            onChange={(e) => setKetAlat(e.target.value)}
                            placeholder="Keterangan"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label text-dark"> Role </label>
                            {admin && admin.name}
                    </div>
                    <div className="field">
                        <div className="control text-dark">
                            <button type="submit" className="button is-success ">Update</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
);
};

export default FormEditAlat;