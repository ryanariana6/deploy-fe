import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Swal from 'sweetalert2';
import moment from 'moment';
import 'moment/locale/id';

const ListService = () => {
    const [Services, setServices] = useState([]);
    
    useEffect(() =>{
        getLayanan();
    },[]);

    const getLayanan = async () => {
        const response = await axios.get('https://ryanariana6.my.id/services');
        setServices(response.data);
    };

    const deleteLayanan = async (serviceId) => { 
        await axios.delete(`https://ryanariana6.my.id/services/${serviceId}`);
        getLayanan();
    };

    const deleteProses  = async (serviceId) => {
        try {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    deleteLayanan(serviceId);
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                }
            })
        } catch (error) {
            
        }
    };
    return (
    <div className="py-4">
        <h1 className="title text-dark"> Layanan </h1>
        <h2 className="subtitle text-dark"> List Layanan </h2>
        <Link to="/admin/services/add" className="button is-primary mb-2">Add New </Link>
        <table className="table is-striped is-fullwidth">
            <thead className="text-dark">
                <tr className="text-dark">
                    <th className="text-dark">No</th>
                    <th className="text-dark">Judul</th>
                    <th className="text-dark">Images</th>
                    <th className="text-dark">keterangan</th>
                    <th className="text-dark">Dibuat Oleh</th>
                    <th className="text-dark">Dibuat Pada</th>
                    <th className="text-dark">Diubah Pada</th> 
                    <th className="text-dark">Actions</th>
                </tr>
            </thead>
            <tbody>
            {Services.map((services, index) => (
                <tr key={services.uuidLayanan}>
                    <td>{index + 1}</td>
                    <td>{services.nameLayanan}</td>
                    <td><img src={`https://ryanariana6.my.id/images/imgLayanan/${services.imageLayanan}`} alt="No images" width="200" height="300"/></td>
                    <td className="text-break"><details>{services.ketLayanan}</details></td>
                    <td>{services.user.name}</td>
                    <td>{moment(services.createdAt).format('dddd, DD MMMM YYYY')}</td>
                    <td>{moment(services.updatedAt).format('DD MMMM YYYY')}</td>
                    <td>
                        <Link to={`/admin/services/edit/${services.id}`} className="button is-small is-info">Edit</Link>
                        <button onClick={()=> deleteProses(services.id)} className="button is-small is-danger">Hapus</button>
                    </td>
                </tr>
                ))}
            </tbody>
        </table>
    </div>
    );
};

export default ListService;