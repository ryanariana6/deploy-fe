import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Swal from 'sweetalert2';
import moment from 'moment';
import 'moment/locale/id';

const ListBerita = () => {
    const [News, setNews] = useState([]);
    
    useEffect(() =>{
        getBerita();
    },[]);

    const getBerita = async () => {
        const response = await axios.get('https://ryanariana6.my.id/news');
        setNews(response.data);
    };

    const deleteBerita = async (newsId) => { 
        await axios.delete(`https://ryanariana6.my.id/news/${newsId}`);
        getBerita();
    };

    const deleteProses  = async (newsId) => {
        try {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    deleteBerita(newsId);
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                }
            })
        } catch (error) {
            
        }
    };
    return (
    <div className="py-4">
        <h1 className="title text-dark"> Berita </h1>
        <h2 className="subtitle  text-dark"> List Berita </h2>
        <Link to="/admin/news/add" className="button is-primary mb-2 text-dark">Add New</Link>
        <table className="table is-striped is-fullwidth">
            <thead className="text-dark">
                <tr className="text-dark">
                    <th className="text-dark">No</th>
                    <th className="text-dark">Judul</th>
                    <th className="text-dark">Images</th>
                    <th className="text-dark">keterangan</th>
                    <th className="text-dark">Dibuat Oleh</th>
                    <th className="text-dark">Dibuat Pada</th>
                    <th className="text-dark">Diubah Pada</th>
                    <th className="text-dark">Actions</th>
                </tr>
            </thead>
            <tbody>
            {News.map((news, index) => (
                <tr key={news.uuidBerita}>
                    <td>{index + 1}</td>
                    <td>{news.nameBerita}</td>
                    <td><img src={`https://ryanariana6.my.id/images/imgBerita/${news.imageBerita}`} alt="No images" width="200" height="100"/></td>
                    <td><details>{news.ketBerita}</details></td>
                    <td>{news.user.name}</td>
                    <td>{moment(news.createdAt).format('dddd, DD MMMM YYYY')}</td>
                    <td>{moment(news.updatedAt).format('DD MMMM YYYY')}</td>
                    <td>
                        <Link to={`/admin/news/edit/${news.id}`} className="button is-small is-info">Edit</Link>
                        <button onClick={()=> deleteProses(news.id)} className="button is-small is-danger">Hapus</button>
                    </td>
                </tr>
                ))}
            </tbody>
        </table>
    </div>
    );
};

export default ListBerita;